﻿using ASPDotNetCore3.Services.Extensions;
using ASPDotNetCore3.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CourseProjectASPDotNETCore3.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IParserSearchString _parserSearchString;

        public IndexModel(IParserSearchString parserSearchString)
        {  
            _parserSearchString = parserSearchString;
        }

        [TempData]
        public string resultFileWrited { get; set; } = string.Empty;

        [BindProperty(SupportsGet = true)]
        public string strsearch { get; set; }
        public void OnGet(string strsearch)
        {
            if (strsearch.IsNullOrEmpty()) return;

            resultFileWrited = _parserSearchString.ParseSearchString(strsearch.Trim());
        }
    }
}
