﻿namespace ASPDotNetCore3.Services.Interfaces
{
    public interface IParserSearchString
    {
        public string WriteFileJson(string pathFile);

        public string ParseSearchString(string stringSearch);
    }
}
