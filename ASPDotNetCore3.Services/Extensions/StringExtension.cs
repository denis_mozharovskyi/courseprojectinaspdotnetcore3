﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPDotNetCore3.Services.Extensions
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string str) => string.IsNullOrEmpty(str);       
    }
}
