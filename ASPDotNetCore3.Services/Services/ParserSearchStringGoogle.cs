﻿using ASPDotNetCore3.Services.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ASPDotNetCore3.Services.Services
{
    public class ParserSearchStringGoogle : IParserSearchString
    {
        private readonly Dictionary<string, string> ReturnFileWrited = 
            new Dictionary<string, string> 
            { 
                ["err"] = "error", 
                ["succ"] = "success" 
            };
        
        private readonly string pathDirJson = @"ParseJsonFiles"; 
        private readonly string nameFileJson = @"parseDomenNames";
        private readonly string nameFileJsonExtension = @"json";
        
        private readonly string urlGoogle = @"https://www.google.com.ua/";
        
        private readonly string keyJObjectParseStr = @"parseSearchStringGoogle";
        private readonly string keyJObjectDomens = @"domens";
        private JObject jObject = new JObject();   
        
        private string BuildPathToJsonFile()
        {
            // Determines if the directory exists at the specified path, if not, then creates
            DirectoryInfo dirInfo = new DirectoryInfo(pathDirJson);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            // Build full path to json file
            string strPathJsonFile = $"{pathDirJson}\\{nameFileJson}_{DateTime.Now.ToFileTime()}.{nameFileJsonExtension}";

            return strPathJsonFile;
        }

        public string WriteFileJson(string pathFile)
        {
            if (jObject[keyJObjectDomens] == null || jObject[keyJObjectDomens].Type == JTokenType.Null)
            {
                return ReturnFileWrited["err"];
            }

            JsonSerializer serializer = new JsonSerializer();   

            // Serialized and writed json string to file
            using (StreamWriter sw = new StreamWriter(pathFile))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.Indented;
                serializer.Serialize(writer, jObject);
            }

            return ReturnFileWrited["succ"];
        }

        private IList<IWebElement> GetWebElementsOnPage(IWebDriver driver, string cssSelector)
        {
            return new List<IWebElement>(driver
                    .FindElements(By.CssSelector(cssSelector))
                    .Where(e => e.Displayed));
        }

        public string ParseSearchString(string stringSearch)
        {
            // Open Chrome browser
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            // Navigate to URL 
            driver.Url = urlGoogle;
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

            // Identify the Google search text box
            IWebElement elementInput = driver.FindElement(By.Name("q"));

            // Enter the value in the google search text box 
            elementInput.SendKeys(stringSearch);

            // Identify the google search button  
            IWebElement elementButton = driver.FindElement(By.Name("btnK"));

            // Click on the Google search button from JavaScript
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", elementButton);

            // Create visible list elements pagination
            var elementsPaginate = GetWebElementsOnPage(driver, "tbody > tr > td > a.fl");
                  
            // Number of pages for parsing (10)
            var pagesCount = elementsPaginate.Count + 1 > 10 ? 10 : elementsPaginate.Count + 1;

            // Array all pages with domen names
            JArray jArray = new JArray();

            for (var i = 1; i <= pagesCount; i++)
            {
                // Array domen names on page
                JArray jArrayPages = new JArray();

                // Create visible list elements link
                var elements = GetWebElementsOnPage(driver, "div.g > div.rc > div.r > a > div.TbwUpd > cite");

                // Domen names on page write in array
                if (elements.Count > 0)
                {
                    foreach (var item in elements)
                    {
                        string[] strs = item.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        jArrayPages.Add(strs[0]);
                    }
                }

                // Object array domen names on page
                JObject jObjectPages = new JObject();
                jObjectPages[$"Page {i}"] = jArrayPages;
                jArray.Add(jObjectPages);

                if (i == pagesCount) break;
                // Link Next page click
                if (pagesCount > 1)
                    driver.FindElement(By.Id("pnnext")).Click();
            }

            // JObject with a property value equal to the search string on Google 
            jObject[keyJObjectParseStr] = stringSearch;

            // JObject with a property value equal to the array of all pages with domain names
            jObject[keyJObjectDomens] = jArray;

            // Close Chrome browser
            driver.Quit();

            // Full path to file
            string pathFullFileJson = BuildPathToJsonFile();

            // Write json file
            string result = WriteFileJson(pathFullFileJson);

            return result;
        }
    }
}
